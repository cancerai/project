package com.cancerai.Controller;

import com.cancerai.Pojo.AccessFolderPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class DeleteFolderController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("deleteFolder")
    public Map deleteFolder(@RequestBody AccessFolderPojo accessFolderPojo) {
        String token = accessFolderPojo.getToken();
        String path = static_path + accessFolderPojo.getPath();

        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }

        // 指定要删除的目录路径
        File directory = new File(path);

        if (directory.isDirectory()) {
            // 调用删除目录的方法
            boolean deleted = deleteDirectory(directory);

            // 输出删除结果
            if (deleted) {
                return Result.success("success", "success");
            } else {
                return Result.error("delete error", "删除失败");
            }
        }
        if (directory.isFile()) {
            boolean bool = directory.delete();
            if (bool) {
                return Result.success("success", "success");
            } else {
                return Result.error("delete error", "删除失败");
            }
        }
        return Result.error("unexpected errors", "未知错误");
    }

    public static boolean deleteDirectory(File directory) {
        if (!directory.exists()) {
            return true;
        }

        // 如果是文件，直接删除
        if (directory.isFile()) {
            return directory.delete();
        }

        // 如果是目录，递归删除所有子项
        File[] files = directory.listFiles();
        if (files != null) {
            for (File file : files) {
                // 递归删除子目录或文件
                if (!deleteDirectory(file)) {
                    return false; // 如果子项删除失败，则返回false
                }
            }
        }

        // 删除空目录
        return directory.delete();
    }

}
