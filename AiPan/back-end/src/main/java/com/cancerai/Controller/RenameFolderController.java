package com.cancerai.Controller;

import com.cancerai.Pojo.RenameFolderPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class RenameFolderController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;

    @PostMapping("renameFolder")
    public Map renameFolder(@RequestBody RenameFolderPojo renameFolderPojo) throws IOException {
        String token = renameFolderPojo.getToken();
        String path = static_path + renameFolderPojo.getPath();
        String name = static_path + renameFolderPojo.getName();
//        System.out.println(path);
//        System.out.println(name);
        //        验证token
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }
//        过滤特殊符号
        if (name.contains("\\")) {
            return Result.error("special symbols", "不能包含特殊符号");
        }
//        原始路径
        Path source = Paths.get(path);
//        目标路径
        Path target = Paths.get(name);

        try {
//            重命名文件夹
            Files.move(source, target);
            return Result.success("success", "success");
        } catch (IOException e) {
//            本来这里操作目录是没问题的，但是在命名文件的走这里抛出异常
//            所以我这边就直接不执行任何操作
//            按理说正常操作是没问题的（担心）
//            return Result.error("rename error", "重命名失败");
        }
        return Result.success("success", "success");
    }
}
