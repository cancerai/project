package com.cancerai.Controller;

import com.cancerai.Pojo.IsLoginPojo;
import com.cancerai.Pojo.Result;
import com.cancerai.Service.FindUserService;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@CrossOrigin(origins = "*")
@RestController
public class GetFilesController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;

    @Autowired
    private FindUserService findUserService;

    @PostMapping("getFiles")
    public Map<String, Object> getFiles(@RequestBody IsLoginPojo isLoginPojo) {
        String token = isLoginPojo.getToken();
        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }
        //验证数据库是否有这个token
        try {
            findUserService.findUserWithToken(token).get(0).getName();
        } catch (IndexOutOfBoundsException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "login");
        }
//        根据token获取用户名
        String name = findUserService.findUserWithToken(token).get(0).getName();

//        获取用户云盘目录路径
        File findUserHome = new File(static_path + "/" + name);
        File[] files = findUserHome.listFiles();

//        准备一大堆list用来存数据
        List<String> fileItems = new ArrayList();
        List<String> directoryItems = new ArrayList();
        List<List> nameList = new ArrayList();

        List<String> fileDate = new ArrayList<>();
        List<String> directoryDate = new ArrayList();
        List<List> dateList = new ArrayList();

        List filePath = new ArrayList();
        List directoryPath = new ArrayList();
        List pathList = new ArrayList();
        List<List> result = new ArrayList();

        if (findUserHome.list().length > 0) {
//            获取文件和目录的名字
            for (File file : files) {
                if (file.isFile()) {
                    fileItems.add(file.getName());
                }
                if (file.isDirectory()) {
                    directoryItems.add(file.getName());
                }
            }
            nameList.add(fileItems);
            nameList.add(directoryItems);

//获取文件和目录的最后修改时间
            for (File file : files) {
                if (file.isFile()) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String lastModifyDate = format.format(file.lastModified());
                    fileDate.add(lastModifyDate);
                }
                if (file.isDirectory()) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                    String lastModifyDate = format.format(file.lastModified());
                    directoryDate.add(lastModifyDate);
                }
            }
//            记录修改日期的列表
            dateList.add(fileDate);
            dateList.add(directoryDate);

            //获取文件和目录的路径
            for (File file : files) {
                if (file.isFile()) {
                    filePath.add(file.getAbsolutePath().replaceFirst(Pattern.quote(static_path), ""));
                }
                if (file.isDirectory()) {
                    directoryPath.add(file.getAbsolutePath().replaceFirst(Pattern.quote(static_path), ""));
                }
            }
//            记录修改日期的列表
            pathList.add(filePath);
            pathList.add(directoryPath);
            result.add(nameList);
            result.add(dateList);
            result.add(pathList);
            return Result.success("success", result);
        } else {
            return Result.success("directory is null", "/" + name);
        }
    }
}
