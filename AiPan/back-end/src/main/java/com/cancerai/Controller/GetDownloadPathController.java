package com.cancerai.Controller;


import com.cancerai.Pojo.GetUserNamePojo;
import com.cancerai.Pojo.Result;
import com.cancerai.Service.FindUserService;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class GetDownloadPathController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;

    @Autowired
    FindUserService findUserService;

    @PostMapping("getDownloadPath")
    public Map getUserName(@RequestBody GetUserNamePojo getUserNamePojo) {
        String token = getUserNamePojo.getToken();
        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port +  "/login");
        }

        return Result.success("success", ip + ":" + front_end_port + "/user");
    }
}
