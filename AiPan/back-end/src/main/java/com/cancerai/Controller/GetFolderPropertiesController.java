package com.cancerai.Controller;

import com.cancerai.Pojo.AccessFolderPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class GetFolderPropertiesController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("getFolderProperties")
    public Map getFolderProperties(@RequestBody AccessFolderPojo accessFolderPojo) {
        String token = accessFolderPojo.getToken();
        String path = static_path + accessFolderPojo.getPath();
        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }

        File folder = new File(path);
        if (folder.isDirectory()) {
            if (!folder.exists()) {
                return Result.error("directory not exists", "当前目录不存在");
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String lastModifyDate = format.format(folder.lastModified());
            Map info = new HashMap();
            double kb = (double) FileUtils.sizeOfDirectory(folder) / 1000;
            double mb = kb / 1000;
            info.put("name", folder.getName());
            info.put("size", mb + "MB");
            info.put("date", lastModifyDate);
            return Result.success("success", info);
        }
        if (folder.isFile()) {
            if (!folder.exists()) {
                return Result.error("file not exists", "当前文件不存在");
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String lastModifyDate = format.format(folder.lastModified());
            Map info = new HashMap();
            double kb = (double) FileUtils.sizeOf(folder) / 1000;
            double mb = kb / 1000;
            info.put("name", folder.getName());
            info.put("size", mb + "MB");
            info.put("date", lastModifyDate);
            return Result.success("success", info);
        }
        return Result.error("unexpected errors", "未知错误");
    }
}
