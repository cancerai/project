package com.cancerai.Controller;

import com.cancerai.Pojo.Result;
import com.cancerai.Pojo.UpdateTextPojo;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class UpdateTextController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("updateText")
    public Map updateText(@RequestBody UpdateTextPojo updateTextPojo) throws IOException, InterruptedException {
        String token = updateTextPojo.getToken();
        String path = updateTextPojo.getPath();
        String text = updateTextPojo.getText();

        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }

        try {
            FileOutputStream fileOutputStream1 = new FileOutputStream(static_path + path, false);
            byte[] b = text.getBytes();
            fileOutputStream1.write(b);
            fileOutputStream1.close();
            return Result.success("success", "保存成功");
        } catch (IOException e) {
            return Result.error("write error", "保存失败");
        }
    }
}
