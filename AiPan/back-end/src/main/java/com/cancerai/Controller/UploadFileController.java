package com.cancerai.Controller;


import com.cancerai.Pojo.Result;
import com.cancerai.Pojo.UploadFilePojo;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RestController
public class UploadFileController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("uploadFile")
    public Map uploadFile(@RequestParam("file") MultipartFile file, @RequestParam String token, @RequestParam String path) throws IOException {
        path = static_path + path;
        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port + "/login");
        }
//      检查文件有没有反斜杠
        if (file.getOriginalFilename().contains("\\")) {
            return Result.error("special symbols", "文件名不能包含特殊符号");
        }
//        添加文件，没有默认覆盖
        file.transferTo(new File(path + "/" + file.getOriginalFilename()));
        return Result.success("success", "success");
    }
}
