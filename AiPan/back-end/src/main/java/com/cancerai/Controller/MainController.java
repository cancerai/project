package com.cancerai.Controller;

import com.cancerai.GetJWT;
import com.cancerai.Pojo.ForgotPassPojo;
import com.cancerai.Pojo.Result;
import com.cancerai.Pojo.UserPojo;
import com.cancerai.Pojo.VerifyUserPojo;
import com.cancerai.Service.AddUserService;
import com.cancerai.Service.DelUserService;
import com.cancerai.Service.FindUserService;
import com.cancerai.Service.ModifyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(value = "*")
@RestController
public class MainController {
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;
    @Value("${static.path}")
    private String static_path;
    @Autowired
    private FindUserService findUserService;
    @Autowired
    private AddUserService addUserService;
    @Autowired
    private DelUserService delUserService;
    @Autowired
    private ModifyUserService modifyUserService;

    //    注册用户
    @PostMapping("register")
    public Map add(@RequestBody VerifyUserPojo verifyUserPojo) {
        String name = verifyUserPojo.getName();
        String pass = verifyUserPojo.getPass();
        String verifyPass = verifyUserPojo.getVerifyPass();
//        判断是否为空
        if (name == "" || pass == "" || verifyPass == "") {
            return Result.error("no context", "请输入账号或密码");
        }
//        判断两次密码对不对
        if (!pass.equals(verifyPass)) {
            return Result.error("password not same", "密码不一致");
        }
//        判断长度
        if (name.length() > 20) {
            return Result.error("name too lang", "用户名长度应在20位以内");
        }
        if (pass.length() < 5) {
            return Result.error("pass too short", "密码长度应大于5位");
        }
        if (pass.length() > 30) {
            return Result.error("pass too lang", "密码长度应在30位以内");
        }
//        判断特殊字符
        for (char c : name.toCharArray()) {
            if (c == '\\' || c == '$' || c == '%' || c == '&' || c == '`' || c == '\"' || c == '\'') {
                return Result.error("name special characters", "用户名不能包含特殊字符");
            }
        }
        for (char c : pass.toCharArray()) {
            if (c == '\\' || c == '$' || c == '%' || c == '&' || c == '`' || c == '\"' || c == '\'') {
                return Result.error("pass special characters", "密码不能包含特殊字符");
            }
        }
//        如果数据库里面已经存在
        if (findUserService.UserIsExist(name) != null) {
            return Result.error("user exist", "用户已存在");
        } else {
//            如果存在user-file目录则直接创建用户根目录，没有则先创建user-file目录
            if (new File(static_path).exists()) {
                //        新建用户目录
                File addUserHome = new File(static_path + name);
                if (!addUserHome.exists()) {
//                创建用户目录
                    if (addUserHome.mkdir()) {
                        addUserHome.mkdir();
//                    添加目录成功
                        //        添加数据到数据库
                        addUserService.addUser(name, pass);
                    } else {
//                    用户目录添加不成功
                        return Result.error("create directory error", "create directory error");
                    }

                } else {
                    return Result.error("user exist cannot create directory", ip + ":" + front_end_port+ "/login/error.html");
                }


                //        验证有没有添加成功
                if (findUserService.UserIsExist(name) == null) {
//                没有查到添加的数据
//                判定为意外错误
                    return Result.error("unexpected errors", ip +":" + front_end_port+ "/login/error.html");
                } else {
                    return Result.success("successful", ip + ":" + front_end_port + "/login/success.html");
                }
            } else {
//                创建user-file目录
                new File(static_path).mkdir();
                //        新建用户目录
                File addUserHome = new File(static_path + name);
                if (!addUserHome.exists()) {
//                创建用户目录
                    if (addUserHome.mkdir()) {
                        addUserHome.mkdir();
//                    添加目录成功
                        //        添加数据到数据库
                        addUserService.addUser(name, pass);
                    } else {
//                    用户目录添加不成功
                        return Result.error("create directory error", "create directory error");
                    }

                } else {
                    return Result.error("user exist cannot create directory", ip + ":" + front_end_port+"/login/error.html");
                }


                //        验证有没有添加成功
                if (findUserService.UserIsExist(name) == null) {
//                没有查到添加的数据
//                判定为意外错误
                    return Result.error("unexpected errors", ip + ":" + front_end_port+"/login/error.html");
                } else {
                    return Result.success("successful", ip + ":" + front_end_port+"/login/success.html");
                }
            }
        }
    }

    //    删除用户
    @PostMapping("delete")
    public Map del(@RequestBody VerifyUserPojo verifyUserPojo) {
        String name = verifyUserPojo.getName();
        String pass = verifyUserPojo.getPass();
        String verifyPass = verifyUserPojo.getVerifyPass();
//        判断传来的数据是否为空
        if (name == "" || pass == "" || verifyPass == "") {
            return Result.error("no context", "请输入账号或密码");
        }
//        判断两次密码对不对
        if (!pass.equals(verifyPass)) {
            return Result.error("password not same", "密码不一致");
        }
//        账号或密码错误
        if (findUserService.findUser(name, pass) == null) {
            return Result.error("user not exist", "账号或密码错误");
        }
//        删除数据库用户
        delUserService.delUser(name);
//        删除用户云盘目录
        File file = new File(static_path + name);
        file.delete();
//        判断用户有没有被删掉
        if (findUserService.findUser(name, pass) != null || file.exists()) {
            return Result.error("delete error", "发生意外错误");
        }
        return Result.success("successful", "注销成功");
    }

    //登陆用户
    @PostMapping("login")
    public Map login(@RequestBody UserPojo userPojo) {
        String name = userPojo.getName();
        String pass = userPojo.getPass();
//        判断传来的数据是否为空
        if (name == "" || pass == "") {
            return Result.error("no context", "请输入账号或密码");
        }
//        没有该账号
        if (findUserService.findUser(name, pass) == null) {
            return Result.error("name or pass error", "账号或密码错误");
        }

//        生成它jwt
        String token = GetJWT.build(name, pass);
//        把jwt存到数据库里，根据用户添加
        findUserService.addToken(token, name);
//        登陆成功给前端返回一个JWT
        Map<String, Object> re = new HashMap<>();
        re.put("url", ip + ":" + front_end_port+"/pan-home");
        re.put("token", token);
        return Result.success("successful", re);
    }

    //修改用户
    @PostMapping("modify")
    public void modify(String modify_name, String name, String pass) {
        modifyUserService.modifyUser(modify_name, name, pass);
    }

    @PostMapping("forgotPass")
    public Map forgotPass(@RequestBody ForgotPassPojo forgotPassPojo) {
        String name = forgotPassPojo.getName();
        if (name.isEmpty()) {
            return Result.error("name is null", "请输入用户名");
        }
//        存在该用户
        if (findUserService.UserIsExist(name) != null) {
            return Result.success("success", ip + ":" + front_end_port+"/login/forgotPassword_pass.html");
        } else {
            return Result.error("user not exists", "用户不存在");
        }
    }

    @PostMapping("forgotPassInput")
    public Map forgotPassInput(@RequestBody VerifyUserPojo verifyUserPojo) {
        String name = verifyUserPojo.getName();
        String pass = verifyUserPojo.getPass();
        String verifyPass = verifyUserPojo.getVerifyPass();
        if (pass.isEmpty() || verifyPass.isEmpty()) {
            return Result.error("value is empty", "请输入密码");
        }
        if (!pass.equals(verifyPass)) {
            return Result.error("password not same", "密码不一致");
        }
        if (pass.length() < 5) {
            return Result.error("pass too short", "密码长度应大于5位");
        }
        if (pass.length() > 30) {
            return Result.error("pass too lang", "密码长度应在30位以内");
        }
        if (pass.contains("\\") || pass.contains("$") || pass.contains("%") || pass.contains("&") || pass.contains("`") || pass.contains("\'") || pass.contains("\"")) {
            return Result.error("special symbol", "不能包含特殊符号");
        }
        if (verifyPass.contains("\\") || verifyPass.contains("$") || verifyPass.contains("%") || verifyPass.contains("&") || verifyPass.contains("`") || verifyPass.contains("\'") || verifyPass.contains("\"")) {
            return Result.error("special symbol", "不能包含特殊符号");
        }
        modifyUserService.modifyUser(name, name, pass);
        return Result.success("success", ip + ":" + front_end_port+"/login/index.html");
    }
}
