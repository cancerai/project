package com.cancerai.Controller;

import com.cancerai.Pojo.AccessFolderPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class EditFileController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("editFile")
    public Map editFile(@RequestBody AccessFolderPojo accessFolderPojo) throws IOException {
        String token = accessFolderPojo.getToken();
        String path = static_path + accessFolderPojo.getPath();
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port +"/login");
        }

//        获取文件所有的行内容
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            return Result.success("success", lines);
        } catch (IOException e) {
            return Result.error("read file error", "读取文件失败");
        }
    }
}
