package com.cancerai.Controller;

import com.cancerai.Pojo.CreateNewFolderPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class CreateNewFolderController {
    @Value("${static.path}")
    private String static_path;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;

    @PostMapping("createNewFolder")
    public Map createNewFolder(@RequestBody CreateNewFolderPojo createNewFolderPojo) {
        String token = createNewFolderPojo.getToken();
        String currentPath = static_path + createNewFolderPojo.getCurrentPath();
        String input = createNewFolderPojo.getInput();

        //验证token是否有效
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("tokne error", ip + ":" + front_end_port +"/login");
        }

        if (input == "") {
            return Result.error("path is null", "请输入目录名字");
        }

//        过滤掉特殊符号
        if (input.contains("/") || input.contains("\\") || input.contains("`") || input.contains("$") || input.contains("%") || input.contains("&")) {
            return Result.error("special symbols", "不能包含特殊符号");
        }
//        创建路径并创建
        File folder = new File(currentPath + "/" + input);
        boolean created = folder.mkdir();

        if (created) {
            return Result.success("success", "success");
        } else {
            return Result.error("create error", "已存在同名的文件或目录");
        }
    }
}
