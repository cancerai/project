package com.cancerai.Controller;

import com.cancerai.Pojo.GetNameWithTokenPojo;
import com.cancerai.Pojo.Result;
import com.cancerai.Pojo.UserPojo;
import com.cancerai.Service.FindUserService;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin({"*"})
@RestController
public class GetNameWithTokenController {
    @Autowired
    private FindUserService findUserService;
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("getNameWithToken")
    public Map getNameWithToken(@RequestBody GetNameWithTokenPojo getNameWithTokenPojo) {
//        获取并定义数据
        String token = getNameWithTokenPojo.getToken();
        String name = "";
//        检验jwt
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(token)
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("token error", ip + ":" + front_end_port + "/login");
        }

        List<UserPojo> list = findUserService.findUserWithToken(token);
        for (UserPojo userPojo : list) {
            name = userPojo.getName();
        }
        if (name == null) {
            return Result.error("name is null", "用户名不存在");
        }
//        返回名字
        return Result.success("success", name);
    }
}
