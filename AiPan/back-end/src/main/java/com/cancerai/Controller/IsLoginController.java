package com.cancerai.Controller;

import com.cancerai.Pojo.IsLoginPojo;
import com.cancerai.Pojo.Result;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@CrossOrigin(origins = {"*"})
@RestController
public class IsLoginController {
    @Value("${server.ip.address}")
    private String ip;
    @Value("${front.end.port}")
    private String front_end_port;


    @PostMapping("isLogin")
    public Map isLogin(@RequestBody IsLoginPojo isLoginPojo) {
        try {
            Jwts.parser()
                    .setSigningKey("JWT_cancerai")
                    .parseClaimsJws(isLoginPojo.getToken())
                    .getBody();
        } catch (RuntimeException e) {
            return Result.error("error", ip + ":" + front_end_port + "/login");
        }
        return Result.success("seccessful", "OK");
    }
}
