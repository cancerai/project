package com.cancerai;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GetJWT {
    public static String build(String name, String pass) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("name", name);
        claims.put("pass", pass);
        String jwt = Jwts.builder()
                .signWith(SignatureAlgorithm.HS256,"JWT_cancerai")
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + (long)7 * 24 * 60 * 60 * 1000))
                .compact();
        return jwt;
    }
}
