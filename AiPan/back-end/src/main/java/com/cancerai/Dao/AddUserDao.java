package com.cancerai.Dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddUserDao {
    @Insert("insert into user_info (name,pass) values(#{name},#{pass})")
    void addUser(String name, String pass);
}
