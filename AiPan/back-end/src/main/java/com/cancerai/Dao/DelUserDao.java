package com.cancerai.Dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DelUserDao {
    @Delete("delete from user_info where name=#{name}")
    void delUser(String name);
}
