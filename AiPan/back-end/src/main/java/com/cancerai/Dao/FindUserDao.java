package com.cancerai.Dao;

import com.cancerai.Pojo.UserPojo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface FindUserDao {
    @Select("select * from user_info where name=#{name} and pass=#{pass}")
    UserPojo findUser(String name, String pass);

    @Select("select * from user_info where name=#{name}")
    UserPojo UserIsExist(String name);

    //    根据用户名修改jwt
    @Update("update user_info set token = #{token} where name = #{name}")
    void addToken(String token, String name);

    //    根据jwt查找用户名
    @Select("select * from user_info where token = #{token}")
    List<UserPojo> findUserWithToken(String token);
}
