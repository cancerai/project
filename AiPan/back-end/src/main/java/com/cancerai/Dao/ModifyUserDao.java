package com.cancerai.Dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface ModifyUserDao {
    @Update("update user_info set name=#{name},pass=#{pass} where name=#{modify_name}")
    void modifyUser(String modify_name, String name, String pass);
}
