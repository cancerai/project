package com.cancerai.Pojo;

import java.util.HashMap;
import java.util.Map;

public class Result {
//    String mess 描述信息
//    Object object 要返回的内容

    public static Map success(String mess, Object object) {
        Map<String, Object> re = new HashMap<>();
        re.put("message", mess);
        re.put("result", object);
        return re;
    }

    public static Map error(String mess, Object object) {
        Map<String, Object> re = new HashMap<>();
        re.put("message", mess);
        re.put("result", object);
        return re;
    }
}
