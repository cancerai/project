package com.cancerai.Pojo;

public class CreateNewFolderPojo {
    String token;
    String currentPath;
    String input;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String currentPath) {
        this.currentPath = currentPath;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public String toString() {
        return "CreateNewFolderPojo{" +
                "token='" + token + '\'' +
                ", currentPath='" + currentPath + '\'' +
                ", input='" + input + '\'' +
                '}';
    }
}
