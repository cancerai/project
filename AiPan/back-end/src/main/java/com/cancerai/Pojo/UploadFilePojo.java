package com.cancerai.Pojo;

import org.springframework.web.multipart.MultipartFile;

public class UploadFilePojo {
    private String token;
    private MultipartFile file;
    private String path;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
