package com.cancerai.Service;

import com.cancerai.Dao.AddUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddUserService implements AddUserDao {
    @Autowired
    private AddUserDao addUserDao;

    @Override
    public void addUser(String name, String pass) {
        addUserDao.addUser(name, pass);
    }
}
