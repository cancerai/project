package com.cancerai.Service;

import com.cancerai.Dao.ModifyUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModifyUserService implements ModifyUserDao{
    @Autowired
    private ModifyUserDao modifyUserDao;

    @Override
    public void modifyUser(String modify_name, String name, String pass) {
        modifyUserDao.modifyUser(modify_name, name, pass);
    }
}
