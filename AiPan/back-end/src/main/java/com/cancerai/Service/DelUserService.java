package com.cancerai.Service;

import com.cancerai.Dao.DelUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DelUserService implements DelUserDao {
    @Autowired
    private DelUserDao delUserDao;
    @Override
    public void delUser(String name) {
        delUserDao.delUser(name);
    }
}
