package com.cancerai.Service;

import com.cancerai.Dao.FindUserDao;
import com.cancerai.Pojo.UserPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FindUserService implements FindUserDao {
    @Autowired
    private FindUserDao findUserDao;

    @Override
    public UserPojo findUser(String name, String pass) {
        return findUserDao.findUser(name, pass);
    }

    @Override
    public UserPojo UserIsExist(String name) {
        return findUserDao.UserIsExist(name);
    }

    @Override
    public void addToken(String token, String name) {
        findUserDao.addToken(token, name);
    }

    @Override
    public List<UserPojo> findUserWithToken(String token) {
        return findUserDao.findUserWithToken(token);
    }
}
