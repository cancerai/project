
// 获取云盘的主体存储部分
let pan_storage_main = document.getElementById("pan_storage_main");
// 获取自定义菜单
let customMenu = document.getElementById("custom_menu");

// 当用户右键时
pan_storage_main.addEventListener("contextmenu", (e) => {
    // 获取页面顶部的两个元素
    let pan_head = document.getElementById("pan_head");
    let pan_top_path = document.getElementById("pan_top_path");
    // 获取文件和目录的所有元素
    let file = document.querySelectorAll(".pan_items_file");
    let folder = document.querySelectorAll(".pan_items_folder");

    // 获取到的是一个数组
    // 遍历数组获取每一个元素，并判断右键的是否是它们
    // 虽然里面 if 里面没写任何东西，但不能删掉
    // 如果删掉不管点击什么都是会根据点击空白处的代码执行
    for (let i = 0; i < file.length; i++) {
        if (file[i].contains(e.target) || e.target == file[i]) {
            return;
        }
    }
    for (let i = 0; i < folder.length; i++) {
        if (folder[i].contains(e.target) || e.target == folder[i]) {
            return;
        }
    }

    // 除了上面两块区域右键不生效，在其他区域右键都生效
    if (pan_head.contains(e.target) || e.target == pan_head) {
        return;
    }
    if (pan_top_path.contains(e.target) || e.target == pan_top_path) {
        return;
    }

    // 判断当前路径是不是根目录
    if (currentPath.replace(/\/[^\/]*$/, "/") === "/") {
        let emptyHtmlCode =
            `<li onclick="showCreateNewFolder()">新建文件夹</li>` +
            `<li onclick="updateBtn()">上传文件</li>` +
            `<li onclick="showFolderProperties(\`${currentPath}\`)">属性</li>` +
            `<li onclick="flushBtn(\`${currentPath}\`)">刷新</li>`;
        showContextMenu(emptyHtmlCode, e);
    } else {
        let emptyHtmlCode =
            `<li onclick="showCreateNewFolder()">新建文件夹</li>` +
            `<li onclick="updateBtn()">上传文件</li>` +
            `<li onclick="showFolderProperties(\`${currentPath}\`)">属性</li>` +
            `<li onclick="flushBtn(\`${currentPath}\`)">刷新</li>` +
            `<li onclick="backFolder(\`${currentPath}\`)">返回上一层</li>`;
        showContextMenu(emptyHtmlCode, e);
    }
})

// 当点击页面的其他任意地方时，关闭右键菜单
document.addEventListener('click', function () {
    customMenu.style.display = 'none';
});