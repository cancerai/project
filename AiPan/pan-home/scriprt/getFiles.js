window.onload = () => {
    // 阻止浏览器默认事件
    // 双击选中
    document.ondragstart = document.onselectstart = function () { return false; };
    // 取消右键
    document.oncontextmenu = function () { return false; }

    // 获取文件数据
    let getFile = new XMLHttpRequest();
    let sendJson = {
        "token": localStorage.getItem("token"),
    }
    getFile.open("post", `${serverIP}:${backEndPort}/getFiles`, true);
    getFile.setRequestHeader("Content-type", "application/json");
    getFile.send(JSON.stringify(sendJson));
    getFile.onreadystatechange = () => {
        if (getFile.readyState == 4 && getFile.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(getFile.responseText).message;
            let result = JSON.parse(getFile.responseText).result;

            // token无效
            if (message == "tokne error") {
                window.location.href = result;
            }

            // 目录为空
            if (message == "directory is null") {
                let htmlCode = "<div>没有更多了</div>";
                let node = document.createElement("div");
                node.innerHTML = htmlCode
                document.getElementById("nothingContainer").appendChild(node);
                // 记录当前目录路径
                currentPath = result;
                console.log(`${currentPath}`);

                // 渲染云盘根目录字样
                showCurrentOnPanTop();
            }

            if (message == "success") {
                // 因为后端传过来的数组格式都是固定的，如果文件或目录有一个没有就会获取不到数据，导致js报错
                // 所以我这边分三种情况判断来进行渲染文件

                // 没有目录，但有文件
                if (result[0][1] == "" && result[0][0] != "") {
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showFiles(result);
                }

                // 没有文件，但有目录
                if (result[0][1] != "" && result[0][0] == "") {
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                }

                // 文件和目录都有
                if (result[0][1] != "" && result[0][0] != "") {
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                    showFiles(result);
                }

                // 渲染云盘根目录字样
                showCurrentOnPanTop();
            }
        }
    }

    // 获取当前登陆的用户名并向 currentUserName 全局变量赋值
    getCurrentUserName(localStorage.getItem("token"));
}