// 检查有没有登陆
let xhr = new XMLHttpRequest();
// 获取localstorage
let jsonData = {
    "token": localStorage.getItem("token"),
}
xhr.open("post", `${serverIP}:${backEndPort}/isLogin`, true);
xhr.setRequestHeader("Content-type", "application/json");
xhr.send(JSON.stringify(jsonData));
xhr.onreadystatechange = () => {
    if (xhr.readyState == 4 && xhr.status == 200) {
        // 获取传过来的数据
        let message = JSON.parse(xhr.responseText).message;
        let result = JSON.parse(xhr.responseText).result;
        // 验证token失败
        if (message == "error") {
            window.location.href = result;
        }
    }
}