// 获取当前登陆的用户名
function getCurrentUserName(token) {
    let xhr = new XMLHttpRequest();
    let sendJson = {
        "token": localStorage.getItem("token"),
    }
    xhr.open("post", `${serverIP}:${backEndPort}/getNameWithToken`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(sendJson));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "token error") {
                window.location.href = result;
            }
            if (message == "name is null") {
                alert(result);
            }
            if (message == "success") {
                currentUserName = result;
            }
        }
    }
}

// 点击一个目录时执行的函数
function itemClick(path) {
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path
    }
    xhr.open("post", `${serverIP}:${backEndPort}/accessFolder`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            // token无效
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "directory is null") {
                // 目录为空
                document.getElementById("showFilesContainer").innerHTML = "";
                document.getElementById("nothingContainer").innerHTML = "<div><div>没有更多了</div></div>";
                // 记录当前目录的路径
                currentPath = result;
                console.log("currentPath: " + result);
            }
            if (message == "success") {
                document.getElementById("nothingContainer").innerHTML = "";
                // 因为后端传过来的数组格式都是固定的，如果文件或目录有一个没有就会获取不到数据，导致js报错
                // 所以我这边分三种情况判断来进行渲染文件

                // 没有目录，但有文件
                if (result[0][1] == "" && result[0][0] != "") {
                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showFiles(result);
                }

                // 没有文件，但有目录
                if (result[0][1] != "" && result[0][0] == "") {

                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                }

                // 文件和目录都有
                if (result[0][1] != "" && result[0][0] != "") {
                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                    showFiles(result);
                }
            }
            showCurrentOnPanTop();
        }
    }
}

// 返回上一层目录
function backFolder(path) {
    let xhr = new XMLHttpRequest();
    // 用字符串替换得到上一层的路径
    path = path.replace(/\/[^\/]*$/, "");
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path
    }
    xhr.open("post", `${serverIP}:${backEndPort}/accessFolder`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            // token无效
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "directory is null") {
                // 目录为空
                document.getElementById("showFilesContainer").innerHTML = "";
                document.getElementById("nothingContainer").innerHTML = "<div><div>没有更多了</div></div>";
                // 记录当前目录的路径
                currentPath = result;
                console.log("currentPath: " + result);
            }
            if (message == "success") {
                document.getElementById("nothingContainer").innerHTML = "";
                // 因为后端传过来的数组格式都是固定的，如果文件或目录有一个没有就会获取不到数据，导致js报错
                // 所以我这边分三种情况判断来进行渲染文件

                // 没有目录，但有文件
                if (result[0][1] == "" && result[0][0] != "") {
                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showFiles(result);
                }

                // 没有文件，但有目录
                if (result[0][1] != "" && result[0][0] == "") {
                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                }

                // 文件和目录都有
                if (result[0][1] != "" && result[0][0] != "") {
                    // 清空原本的内容
                    document.getElementById("showFilesContainer").innerHTML = "";
                    showDirectorys(result);
                    showFiles(result);
                }
            }
            showCurrentOnPanTop();
        }
    }
}

// 刷新
function flushBtn(path) {
    document.getElementById("showFolderPropertiesModalMain").style.display = "none";
    document.getElementById("renameModalMain").style.display = "none";
    document.getElementById("modalMain").style.display = "none";
    document.getElementById("editFileModalMain").style.display = "none";
    itemClick(path);
}

// 点击上传文件的按钮
function updateBtn() {
    let element = document.createElement("input");
    element.setAttribute("type", "file");
    element.click();
    // 当选择完文件再执行
    element.addEventListener("change", () => {
        // 创建文件对象
        let file = new FormData();
        file.append("file", element.files[0]);
        file.append("token", localStorage.getItem("token"));
        file.append("path", currentPath);
        // 发送文件
        let xhr = new XMLHttpRequest();
        xhr.open("post", `${serverIP}:${backEndPort}/uploadFile`, true);
        xhr.send(file);
        xhr.onreadystatechange = () => {
            if (xhr.readyState == 4 && xhr.status == 200) {
                let message = JSON.parse(xhr.responseText).message;
                let result = JSON.parse(xhr.responseText).result;
                if (message == "tokne error") {
                    window.location.href = result;
                }
                if (message == "special symbols") {
                    alert(result);
                }
                if (message == "success") {
                    itemClick(currentPath);
                }
            }
        }
    })
}

// 显示创建目录的提示框
function showCreateNewFolder() {
    // 先让他显示出来
    let getElement = document.getElementById("modalMain");
    // 初始化位置
    getElement.style.top = "0%";
    getElement.style.left = "50%";
    getElement.style.transform = "translate(-50%, 0%)";
    getElement.style.display = "block";
    document.getElementById("createNewFolderInput").value = "";
    document.getElementById("createNewFolderInput").focus();
}

// 新建目录
function createNewFolder(path) {
    let input = document.getElementById("createNewFolderInput").value;
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "currentPath": path,
        "input": input
    }
    xhr.open("post", `${serverIP}:${backEndPort}/createNewFolder`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "path is null") {
                alert(result);
            }
            if (message == "special symbols") {
                alert(result);
            }
            if (message == "create error") {
                document.getElementById("createNewFolderInput").focus();
                alert(result);
            }
            if (message == "success") {
                document.getElementById("modalMain").style.display = "none";
                flushBtn(path);
            }
        }
    }
}

// 点击提示框关闭按钮关闭
function closeModal() {
    document.getElementById("modalMain").style.display = "none";
}

function editFileCloseModal() {
    document.getElementById("editFileModalMain").style.display = "none";
}

// 显示目录属性
function showFolderProperties(path) {
    // 先让他显示出来
    let getElement = document.getElementById("showFolderPropertiesModalMain");
    // 初始化位置
    getElement.style.top = "50%";
    getElement.style.left = "50%";
    getElement.style.display = "block";
    // 点击关闭按钮关闭
    document.getElementById("showFolderPropertiesModalCloseBtn").addEventListener("click", () => {
        document.getElementById("showFolderPropertiesModalMain").style.display = "none";
    })
    dragElement(document.getElementById("showFolderPropertiesModalMain"));

    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path
    }
    xhr.open("post", `${serverIP}:${backEndPort}/getFolderProperties`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;

            if (message === "tokne error") {
                window.location.href = result
            }
            if (message === "directory not exists") {
                alert(result);
            }
            if (message === "success") {
                let name = result.name;
                let size = result.size;
                let date = result.date;
                document.getElementById("folderPropertiesName").innerText = name;
                document.getElementById("folderPropertiesSize").innerText = size;
                document.getElementById("folderPropertiesDate").innerText = date;
            }
        }
    }
}

// 删除目录或文件
function deleteFolder(path) {
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path
    }
    xhr.open("post", `${serverIP}:${backEndPort}/deleteFolder`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;

            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "delete error") {
                alert(result);
            }
            if (message == "unexpected errors") {
                alert(result);
            }
            if (message == "success") {
                flushBtn(path.replace(/\/[^\/]*$/, ""));
                return;
            }
        }
    }
}

// 当右键的是文件时
function showFileContextMenu(event) {
    // 文件的名字
    let name;
    // 根据点击的元素找target里面的属性
    if (event.target.localName == "img") {
        name = event.target.nextElementSibling.lastChild.data;
    }
    if (event.target.id === "showFilesDate") {
        name = event.target.previousSibling.innerText;
    }
    if (event.target.id === "showFilesName") {
        name = event.target.innerText;
    }

    oncontextmenuPath = currentPath + "/" + name;
    let fileHtmlCode =
        `<li onclick="downloadFile(\`${name}\`)">下载</li>` +
        // `<li onclick="showEditFile(\`${oncontextmenuPath}\`)">编辑文件</li>` +
        `<li onclick="showFolderProperties(\`${oncontextmenuPath}\`)">属性</li>` +
        `<li onclick="deleteFolder(\`${oncontextmenuPath}\`)">删除</li>` +
        `<li onclick="showRenameFolder(\`${name}\`)">重命名</li>`;
    document.getElementById("custom_menu").innerHTML = "";
    let customMenu = document.getElementById("custom_menu");
    let ulDOM = document.createElement("ul");
    ulDOM.innerHTML = fileHtmlCode;
    document.getElementById("custom_menu").appendChild(ulDOM);
    // 显示右键菜单
    customMenu.style.display = 'block';
    // 获取菜单的宽高
    let menuWidth = customMenu.offsetWidth;
    let menuHeight = customMenu.offsetHeight;
    // 获取右键时的坐标
    let x = event.clientX;
    let y = event.clientY;
    // 检测并调整位置以防止溢出
    if (x + menuWidth > innerWidth) {
        x -= menuWidth;
    }
    if (y + menuHeight > innerHeight) {
        y -= menuHeight;
    }
    customMenu.style.left = x + 'px';
    customMenu.style.top = y + 'px';
}

// 当右键的是目录时
function showFolderContextMenu(event) {
    let name;
    // 根据点击的元素找target里面的属性
    if (event.target.localName == "img") {
        name = event.target.nextElementSibling.lastChild.data;
    }
    if (event.target.id === "showFilesDate") {
        name = event.target.previousSibling.innerText;
    }
    if (event.target.id === "showFilesName") {
        name = event.target.innerText;
    }

    oncontextmenuPath = currentPath + "/" + name;

    let fileHtmlCode =
        `<li onclick="showFolderProperties(\`${oncontextmenuPath}\`)">属性</li>` +
        `<li onclick="deleteFolder(\`${oncontextmenuPath}\`)">删除</li>` +
        `<li onclick="showRenameFolder(\`${name}\`)">重命名</li>`;
    document.getElementById("custom_menu").innerHTML = "";
    let customMenu = document.getElementById("custom_menu");
    let ulDOM = document.createElement("ul");
    ulDOM.innerHTML = fileHtmlCode;
    document.getElementById("custom_menu").appendChild(ulDOM);
    // 显示右键菜单
    customMenu.style.display = 'block';
    // 获取菜单的宽高
    let menuWidth = customMenu.offsetWidth;
    let menuHeight = customMenu.offsetHeight;
    // 获取右键时的坐标
    let x = event.clientX;
    let y = event.clientY;
    // 检测并调整位置以防止溢出
    if (x + menuWidth > innerWidth) {
        x -= menuWidth;
    }
    if (y + menuHeight > innerHeight) {
        y -= menuHeight;
    }
    customMenu.style.left = x + 'px';
    customMenu.style.top = y + 'px';
}

// show rename window
function showRenameFolder(name) {
    // 显示窗口
    document.getElementById("renameInput").value = name;
    document.getElementById("renameModalMain").style.display = "block";
    document.getElementById("renameInput").focus();
    // 当点击重命名的确认按钮时
    document.getElementById("renameButton").addEventListener("click", () => {
        renameFolder(name);
    })
}

// rename
function renameFolder(name) {
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": `${currentPath}/${name}`,
        "name": `${currentPath}/${document.getElementById("renameInput").value}`
    }
    xhr.open("post", `${serverIP}:${backEndPort}/renameFolder`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "special symbols") {
                alert(result);
            }
            if (message = "success") {
                document.getElementById("renameModalMain").style.display = "none";
                itemClick(currentPath);
                return;
            }
        }
    }
}

// 下载文件
function downloadFile(name) {
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
    }
    xhr.open("post", `${serverIP}:${backEndPort}/getDownloadPath`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // 获取传过来的数据
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message = "success") {
                let a = document.createElement("a");
                a.setAttribute("href", `${result}${currentPath}/${name}`);
                a.setAttribute("download", name);
                a.click();
            }
            return;
        }
    }
}

// 拖动元素
function dragElement(element) {
    //使 DIV 元素拖动：
    dragElement(element);

    function dragElement(elmnt) {
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        /* 将 DIV 从 DIV 内的任何位置移动：*/
        elmnt.onmousedown = dragMouseDown;

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // 在启动时获取鼠标光标位置：
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // 当光标移动时调用一个函数：
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // 计算新的光标位置：
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // 设置元素的新位置：
            elmnt.style.top = `${elmnt.offsetTop - pos2}px`;
            elmnt.style.left = `${elmnt.offsetLeft - pos1}px`;
        }

        function closeDragElement() {
            /* 释放鼠标按钮时停止移动：*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
}

// 退出登陆
function logoutUser() {
    if (confirm("确定退出登陆吗")) {
        localStorage.removeItem("token");
        window.location.href = `${serverIP}/login/index.html`;
    }
}

// 定义一个显示菜单的函数
function showContextMenu(htmlCode, e) {
    document.getElementById("custom_menu").innerHTML = "";
    let ulDOM = document.createElement("ul");
    ulDOM.innerHTML = htmlCode;
    document.getElementById("custom_menu").appendChild(ulDOM);
    // 显示右键菜单
    customMenu.style.display = 'block';
    // 获取菜单的宽高
    let menuWidth = customMenu.offsetWidth;
    let menuHeight = customMenu.offsetHeight;
    // 获取右键时的坐标
    let x = e.clientX;
    let y = e.clientY;
    // 检测并调整位置以防止溢出
    if (x + menuWidth > innerWidth) {
        x -= menuWidth;
    }
    if (y + menuHeight > innerHeight) {
        y -= menuHeight;
    }
    customMenu.style.left = `${x}px`;
    customMenu.style.top = `${y}px`;
}

// 显示编辑文件的框框
function showEditFile(path) {
    document.getElementById("editFileTextarea").value = "";
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path,
    }
    xhr.open("post", `${serverIP}:${backEndPort}/editFile`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "read file error") {
                alert(result);
                itemClick(currentPath);
            }
            if (message == "success") {
                let text;
                for (let i = 0; i < result.length; i++) {
                    text += result[i];
                    if (i == result.length - 1) {
                        break;
                    }
                    text += "\n";
                }
                // 不知道为什么js输出开头总是有undefined，所以这里把它去掉
                text = text.substring(0, 9).replace(/./g, "") + text.substring(9);
                document.getElementById("editFileTextarea").value = text;
                document.getElementById("editFileModalMain").style.display = "block";
            }
        }
    }
}

// 点击编辑文本的更新按钮
function editTextDone(path) {
    let getText = document.getElementById("editFileTextarea").value;
    let xhr = new XMLHttpRequest();
    let jsonData = {
        "token": localStorage.getItem("token"),
        "path": path,
        "text": getText
    }
    xhr.open("post", `${serverIP}:${backEndPort}/updateText`, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(jsonData));
    xhr.onreadystatechange = () => {
        if (xhr.readyState == 4 && xhr.status == 200) {
            let message = JSON.parse(xhr.responseText).message;
            let result = JSON.parse(xhr.responseText).result;
            if (message == "tokne error") {
                window.location.href = result;
            }
            if (message == "write error") {
                alert(result);
            }
            if (message == "success") {
                alert(result);
            }
        }
    }
}

// 渲染文件
function showFiles(result) {
    // 获取名字列表
    let nameList = result[0][0];
    // 获取日期列表
    let dateList = result[1][0];
    // 获取路径列表
    let pathList = result[2][0];

    // 渲染文件
    for (let i = 0; i < nameList.length; i++) {
        let htmlCode =
            `<img src="./resources/file.png"  height="100rem" width="119rem">` +
            `<span id="showFilesName">${nameList[i]}</span>` +
            `<span id="showFilesDate">${dateList[i]}</span>`;
        let node = document.createElement("div");
        node.title = nameList[i];
        node.setAttribute("data-path", pathList[i]);
        node.setAttribute("class", "pan_items_file");
        // 当元素右键
        node.setAttribute("oncontextmenu", "showFileContextMenu(event)")
        node.innerHTML = htmlCode
        document.getElementById("showFilesContainer").appendChild(node);
    }
    // 记录当前目录的路径
    currentPath = pathList[0].replace(/\/[^\/]*$/, "");
    console.log("currentPath: " + currentPath);
}

// 渲染目录
function showDirectorys(result) {
    // 获取名字列表
    let nameList = result[0][1];
    // 获取日期列表
    let dateList = result[1][1];
    // 获取路径列表
    let pathList = result[2][1];
    // 渲染目录
    for (let j = 0; j < nameList.length; j++) {
        let htmlCode =
            `<img src="./resources/folder.png"  height="100rem" width="119rem">` +
            `<span id="showFilesName">${nameList[j]}</span>` +
            `<span id="showFilesDate">${dateList[j]}</span>`;
        let node = document.createElement("div");
        node.title = nameList[j];
        node.setAttribute("data-path", pathList[j]);
        node.setAttribute("class", "pan_items_folder");
        node.setAttribute("onclick", `itemClick("${pathList[j]}")`);
        // 当元素右键
        node.setAttribute("oncontextmenu", "showFolderContextMenu(event)");
        node.innerHTML = htmlCode
        document.getElementById("showFilesContainer").appendChild(node);
    }
    // 记录当前目录的路径
    currentPath = pathList[0].replace(/\/[^\/]*$/, "");
    console.log("currentPath: " + currentPath);
}

// 在网盘上面显示当前用户所在路径
function showCurrentOnPanTop() {
    document.getElementById("page_title").innerHTML = `<p class="title_path_sign" data-path=${currentPath}>云盘: ${currentPath}</p>`;
}

// 统计网盘里文件和目录的数量
// 这个一直达不到效果，先不写了
// function recordePanContentNumber() {
//     let htmlCode =
//         `<div class="checkbox--GfceW checkbox-container--t0ALJ" role="checkbox"` +
//         `aria-checked="false" data-checked="false" data-partial="true"` +
//         `data-disabled="false" data-no-padding="false">` +
//         `<div class="checkbox--53Gjo">` +
//         `<input class="input--hG1Du" type="checkbox" readonly="" value="" checked>` +
//         `</div>` +
//         `</div>`;
//     let panContentNumber = document.getElementById("showFilesContainer").childElementCount;
//     document.getElementById("panContentNumber").innerHTML = `${htmlCode}共 ${panContentNumber} 项`;
// }